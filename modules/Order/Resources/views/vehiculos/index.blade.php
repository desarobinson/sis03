@extends('tenant.layouts.app')

@section('content')

    <tenant-vehiculos-index :type-user="{{json_encode(Auth::user()->type)}}"></tenant-vehiculos-index>

@endsection
